export class CreateProductDto {
  name: string;
  price: string;
  type: string;
  image: string;
}
